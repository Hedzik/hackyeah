using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanHolder : MonoBehaviour, ILevelFinishConstraint
{
 
    public float wateringTime = 3f;
    private float timer = 0;
    public int choresCounter = 0;

    public ParticleSystem wateringEffect;
    public GameObject progressbar;
    public Transform progressbarHandler;

    public WaterableFlower flower;

    public bool IsPassing() => choresCounter >= 2;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "flower") {
            flower = collision.GetComponent<WaterableFlower>();
            if (flower.IsWatered)
                flower = null;
        }


    }

    private void Update()
    {

        if (timer >= wateringTime) {
            choresCounter++;
            wateringEffect.Stop();
            progressbarHandler.localScale = Vector3.one;
            progressbar.SetActive(false);
            flower.Water();
            return;
        }
        if (Input.GetKey(KeyCode.Space) && flower!= null)
        {
            timer += Time.deltaTime;
            progressbarHandler.localScale= new Vector3( timer / wateringTime,1,1);
            if (!wateringEffect.isPlaying)
            {
                progressbar.SetActive(true);
                wateringEffect.Play();
                
            }
        }

        
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "flower") {
            timer = 0;
            wateringEffect.Stop();
            progressbarHandler.localScale = Vector3.one;
            progressbar.SetActive(false);
            flower = null;
        }
    }
}

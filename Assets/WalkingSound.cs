using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingSound : MonoBehaviour
{
    AudioSource AS;
    public bool Moving = false;
    movement mv;
    void Start()
    {
        AS = gameObject.GetComponent<AudioSource>();
        mv = gameObject.GetComponent<movement>();

    }

    // Update is called once per frame
    void Update()
    {
        if (mv.movem != 0 )
        {
            Moving = true;
        }
        else { Moving = false; }
        if (Moving)
        {
            if (!AS.isPlaying) { AS.Play(); AS.time = 0.3f; }
        }
        else { AS.Stop(); }
    }
}

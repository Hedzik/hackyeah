using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro : MonoBehaviour
{
    public GameObject SceneLoader;
    public string Direction;
    Loader load;
    // Start is called before the first frame update
    void Start()
    {
        load = SceneLoader.GetComponent<Loader>();
    }

    public void OnPress()
    {
        load.Transition(Direction,"a");
    }
}

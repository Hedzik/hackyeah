using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public interface ILevelFinishConstraint
{
    bool IsPassing();
}

public class DoorInteraction : MonoBehaviour
{
    private GameObject door = null;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        
        if (collider.tag == "door")
           door = collider.gameObject;
    }


    private void OnTriggerExit2D(Collider2D collider)
    {
        
        if (collider.tag == "door")
            door = null;
    }

    private void Update()
    {
        if (Input.GetKeyDown("space") &&
            door != null)
        {
               if (!TryGetComponent(out ILevelFinishConstraint constraint)) {
                door.GetComponent<DoorPortal>().Enter();
                return;
            }

            
            if(constraint.IsPassing())
                door.GetComponent<DoorPortal>().Enter();

        }
    }
}

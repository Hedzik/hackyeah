using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterableFlower : MonoBehaviour
{
    public Sprite watered;
    public bool IsWatered { private set;  get; } = false;

    public void Water()
    {
        IsWatered = true;
        GetComponent<SpriteRenderer>().sprite = watered;
    }
}

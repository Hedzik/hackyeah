using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
    public Animator sceneAnimator;

    public void Transition(string targetSceneName, string targetDoorId)
    {
        StartCoroutine(proceedTransition(targetSceneName, targetDoorId));
    }

    private IEnumerator proceedTransition(string targetSceneName, string targetDoorId)
    {
        sceneAnimator.SetTrigger("SceneStart");
        SceneManager.LoadScene(targetSceneName);

        yield return new WaitForSeconds(1);
    }


}

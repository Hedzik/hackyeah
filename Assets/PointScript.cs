using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointScript : MonoBehaviour
{
    public GameObject Player;
    public Camera mainCamera;
    // Start is called before the first frame update
    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    private void Update()
    {
        if(transform.position.y<mainCamera.transform.position.y)
        {
            PointsCollecting PC = Player.GetComponent<PointsCollecting>();
            PC.lives--;
            Destroy(gameObject);
        }
    }
   
    /*private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Point")
        { 
            PointsCollecting PC = Player.GetComponent<PointsCollecting>();
            PC.lives--;
            Destroy(collision);
        }
        
    }*/
    
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JohnAsking : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject player;
    public Image Question;
    public Text text;
    public Text Speaker;
    public Button YES;public Text YesText;
    public Button NO;public Text NoText;

    movement move;
    PointsCollecting PC;
    void Start()
    {
        move = player.GetComponent<movement>();
        PC = player.GetComponent<PointsCollecting>();
        Question.enabled = false;Speaker.enabled = false;text.enabled = false;
        YES.enabled = false;NO.enabled = false;YesText.enabled = false;NoText.enabled = false;
    }

    // Update is called once per frame
    public void AskQuestion()
    {
        Debug.Log("JohnAskQuestion");
        move.canMove = false;YesText.enabled = true;NoText.enabled = true;
        Question.enabled = true; Speaker.enabled = true; text.enabled = true;YES.enabled = true;NO.enabled = true;
        text.text = "Congratulations! You scored " + PC.score + " out of 10! Do you want to try one more time?";
    }
}

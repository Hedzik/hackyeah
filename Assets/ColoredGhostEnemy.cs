using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GhostColor {
    Red,
    Blue,
    Yellow
}

public class ColoredGhostEnemy : MonoBehaviour
{
    public Rigidbody2D rb;
    public Light pointLight;
    public GhostColor color;    
    public Vector2 direction;
    public Animator animator;

    public float Speed = 20f;

    private void Start()
    {
        rb.velocity = direction * Speed;
    }

    public void ColorChange()
    {
        color = getDesiredColor();
        animator.SetInteger("color", ((int)color));
        
;    }


    GhostColor getDesiredColor()
    {
        if (color == GhostColor.Yellow)
            return 0;
        return color + 1;
    }
}

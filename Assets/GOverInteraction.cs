using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GOverInteraction : MonoBehaviour
{
    private GameObject GOver = null;
    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="GOver")
        { GOver = collision.gameObject;
            GOver gover = GOver.GetComponent<GOver>();
            gover.Reload();
        }
        
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag=="GOver")
        {
            GOver = null;
        }
    }

    // Update is called once per frame
}

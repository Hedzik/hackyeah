using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsOnClick : MonoBehaviour
{
    public GameObject Loader;
    Loader load;
    // Start is called before the first frame update
    private void Start()
    {
        load = Loader.GetComponent<Loader>();
    }
    public void RestartScene()
    {
        load.Transition("libraryscene", "adhd");
    }
    public void BackToMenu()
    {
        load.Transition("Room2b", "adhdb");
    }
}

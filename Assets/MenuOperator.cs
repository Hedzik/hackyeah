using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOperator : MonoBehaviour
{
    public GameObject ScreenLoader;
    Loader Loader;
    void Start()
    {
        Loader = ScreenLoader.GetComponent<Loader>();
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void Room2()
    {
        Loader.Transition("Room2a", "adhda");
    }
    public void Room1()
    {
        Loader.Transition("Room1a", "autisma");
    }
    public void music()
    {
        GameObject MPlayer;
        MPlayer = GameObject.FindGameObjectWithTag("Finish");
        AudioSource AS=MPlayer.GetComponent<AudioSource>();
        if (!AS.isPlaying) { AS.Play(); }
        else { AS.Stop(); }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JohnInteract : MonoBehaviour
{
    // Start is called before the first frame update
    private GameObject John = null;
    private void Start()
    {
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="John")
        {
            John = collision.gameObject;
            JohnAsking JA = John.GetComponent<JohnAsking>();
            JA.AskQuestion();
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostMinigame : MonoBehaviour
{
    public float interval = 3;
    public List<ColoredGhostEnemy> ghosts = new List<ColoredGhostEnemy>();
     

    // Start is called before the first frame update
    void Start()
    {   
        StartCoroutine(LightSwitcher());
    }

    private IEnumerator LightSwitcher()
    {
        for (; ; ) {

            yield return new WaitForSeconds(interval);
            foreach (var ghost in ghosts)
            {
                ghost.ColorChange();

            }
        }
    }

}

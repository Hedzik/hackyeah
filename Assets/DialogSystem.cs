using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sentence {
    public string Message { get; set; }
    public string Author { get; set; }
}

public class DialogSystem : MonoBehaviour
{
    public Queue<Sentence> sentences = new Queue<Sentence>();
    public Animator animator;
    public Text textField;
    public Text authorNamePlaceholder;

    private void Start()
    {
        sentences.Enqueue(new Sentence { Message = "piotrus koziol no pieknie", Author = "orzel" });
        sentences.Enqueue(new Sentence { Message = "piotrus koziol no niezbyt pieknie", Author = "orzel" });
        Say();
    }

    public void Say()
    {
        if (sentences.Count == 0)
        {
            animator.SetBool("WindowOpen", false);
            return;
        }
        var sent = sentences.Dequeue();
        textField.text = sent.Message;
        authorNamePlaceholder.text = sent.Author;
        animator.SetBool("WindowOpen", true);
    }

    public void Hide()
    {
        animator.SetBool("WindowOpen", false);
    }

    public void SentenceSkip()
    {
        Say();
    }

}

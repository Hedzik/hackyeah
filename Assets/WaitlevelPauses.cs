using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitlevelPauses : MonoBehaviour
{
    // Start is called before the first frame update
    public int howlong = 7;
    public GameObject SceneLoader;
    public string Scene;
    public string Doors;
    Loader Loader;
    void Start()
    {
        Loader = SceneLoader.GetComponent<Loader>();
        StartCoroutine(Poczekaj());

    }
    IEnumerator Poczekaj()
    {
        yield return new WaitForSeconds(howlong);
        Loader.Transition(Scene, Doors);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class DoorPortal : MonoBehaviour
{
    public static List<DoorPortal> list = new List<DoorPortal>();

    public void Start()
    {
        list.Add(this);
    }

    public Loader sceneLoader;
    public string doorId;
    public string targetDoorId;
    public string targetSceneName;

    public void Enter()
    {
        sceneLoader.Transition(targetSceneName, targetDoorId);
    }

}

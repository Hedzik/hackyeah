using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsCollecting : MonoBehaviour
{
    public int lives = 3;
    public int score = 0;
    private GameObject Point = null;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="Point")
        {
            Point = collision.gameObject;
            score++;
            Destroy(Point);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

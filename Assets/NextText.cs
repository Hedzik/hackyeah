using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NextText : MonoBehaviour
{
    public GameObject SceneLoader;
    Loader loader;
    public Canvas C1;
    public Canvas C2;
    // Start is called before the first frame update
    void Start()
    {
        loader = SceneLoader.GetComponent<Loader>();
        C2.enabled = false;
    }

    // Update is called once per frame
    public void B1()
    {
        C2.enabled = true;
        C1.enabled = false;
    }
    public void B2()
    {
        loader.Transition("libraryscene", "adhd");
    }
}
